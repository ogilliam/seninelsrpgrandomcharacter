package strongspearstrong.sentinelsautogen.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class JsonLoader {
    public String getJsonFromFile(String file) {
        ClassPathResource cpr = new ClassPathResource(file);
        try {
            byte[] bdata = FileCopyUtils.copyToByteArray(cpr.getInputStream());
            return new String(bdata, StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.out.println("Failed to find file" + e);
            return "";
        }
    }
}
