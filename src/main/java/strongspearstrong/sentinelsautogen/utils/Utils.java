package strongspearstrong.sentinelsautogen.utils;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Principle;
import strongspearstrong.sentinelsautogen.collections.PowersCollection;
import strongspearstrong.sentinelsautogen.collections.heroes.PrinciplesCollection;
import strongspearstrong.sentinelsautogen.collections.QualitiesCollection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Utils {

    public Principle getPrinciple(String category) {
        List<Principle> availPrinc = PrinciplesCollection.getPrinciplesByCat(category);
        Collections.shuffle(availPrinc, Rolling.getRand());
        Principle princ = availPrinc.get(0);

        if (princ.getName().contains("[energy/element]")) {
            List<Attribute> elements = PowersCollection.getPowersByCat("elemental/energy");
            Collections.shuffle(elements, Rolling.getRand());
            princ.setName(princ.getName().replace("[energy/element]", elements.get(0).getName()));
        }

        return princ;
    }

    //takes a list of the hero's current attributes, a master list of available attributes from the current step, and the type.  Returns a shuffled list of attributes not currently held by the hero
    public List<Attribute> getNewAvailableAttributes(List<Attribute> heroAtribs, List<String> masterList, String atribType) {
        List<Attribute> availableAttributes = attributesExtraction(masterList, atribType);
        availableAttributes.removeAll(heroAtribs);

        Collections.shuffle(availableAttributes, Rolling.getRand());
        return availableAttributes;
    }

    //Takes a list of the hero's current attributes and a list of available attributes.  Returns a shuffled cross section of attributes on the hero and the current step's list
    public List<Attribute> getUseAvailableAttributes(List<Attribute> heroAtribs, List<String> masterList, String atribType) {
        List<Attribute> availableAttributes = attributesExtraction(masterList, atribType);
        availableAttributes.retainAll(heroAtribs);

        Collections.shuffle(availableAttributes, Rolling.getRand());
        return availableAttributes;
    }

    //takes a list of powers or qualities from a string list and compares them to atrib collection to extract a full list of attributes
    public List<Attribute> attributesExtraction(List<String> derivedList, String atribType) {
        List<Attribute> atribList = new ArrayList<>();

        for (String s : derivedList) {
            if (s.contains("category")) {
                atribList.addAll(atribType.equals(AttributeTypes.POWER)
                        ? PowersCollection.getPowersByCat(s.replace("category", "").trim())
                        : QualitiesCollection.getQualitiesByCat(s.replace("category", "").trim()));
            } else {
                atribList.add(atribType.equals(AttributeTypes.POWER)
                        ? new Attribute(s, PowersCollection.getPowerByName(s).getCategory())
                        : new Attribute(s, QualitiesCollection.getQualityByName(s).getCategory()));
            }
        }

        return atribList;
    }


    //Takes an ability and a list of Powers and Qualities that could be applied to it.  Returns a new ability
    public Ability parseAbility(Ability abl, List<Attribute> availablePowers, List<Attribute> availableQualities) {
        Ability newAbl = new Ability(abl.getName());
        try {
            Collections.shuffle(availablePowers, Rolling.getRand());
            Collections.shuffle(availableQualities, Rolling.getRand());

            if (abl.getPowerSelection().equals("any") && abl.getQualitySelection().equals(abl.getPowerSelection())) {
                List<Attribute> combinedSelection = new ArrayList<>(availableQualities);
                combinedSelection.addAll(availablePowers);
                Collections.shuffle(combinedSelection, Rolling.getRand());
                if (PowersCollection.getPowers().stream().map(Attribute::getCategory).collect(toList()).contains(combinedSelection.get(0).getCategory())) {
                    newAbl.setPowerSelection(combinedSelection.get(0).getName());
                } else {
                    newAbl.setQualitySelection(combinedSelection.get(0).getName());
                }
            }
            if (!abl.getPowerSelection().isEmpty() && !abl.getQualitySelection().equals(abl.getPowerSelection())) {
                if (abl.getPowerSelection().equals("any")) {
                    newAbl.setPowerSelection(availablePowers.get(0).getName());
                } else if (abl.getPowerSelection().equals("elemental/energy any")) {
                    List<Attribute> elems = new ArrayList<>(PowersCollection.getPowersByCat("elemental/energy"));
                    Collections.shuffle(elems, Rolling.getRand());
                    newAbl.setPowerSelection(elems.get(0).getName());
                } else if (abl.getPowerSelection().contains("category")) {
                    List<Attribute> powCat = new ArrayList<>(PowersCollection.getPowersByCat(abl.getPowerSelection().replace("category", "").trim()));
                    powCat.retainAll(availablePowers);
                    Collections.shuffle(powCat, Rolling.getRand());
                    newAbl.setPowerSelection(powCat.get(0).getName());
                } else {
                    newAbl.setPowerSelection(abl.getPowerSelection());
                }
            }
            if (!abl.getQualitySelection().isEmpty() && !abl.getQualitySelection().equals(abl.getPowerSelection())) {
                if (abl.getQualitySelection().equals("any")) {
                    newAbl.setQualitySelection(availableQualities.get(0).getName());
                } else if (abl.getQualitySelection().contains("category")) {
                    if (abl.getQualitySelection().contains(" or ")) {
                        String[] options = abl.getQualitySelection().split(" or ");
                        List<Attribute> qualCats = new ArrayList<>(QualitiesCollection.getQualitiesByCat(options[0].replace("category", "").trim()));
                        qualCats.addAll(QualitiesCollection.getQualitiesByCat(options[1].replace("category", "").trim()));
                        qualCats.retainAll(availableQualities);
                        Collections.shuffle(qualCats, Rolling.getRand());
                        newAbl.setQualitySelection(qualCats.get(0).getName());
                    } else {
                        List<Attribute> qualCat = new ArrayList<>(QualitiesCollection.getQualitiesByCat(abl.getQualitySelection().replace("category", "").trim()));
                        qualCat.retainAll(availableQualities);
                        Collections.shuffle(qualCat, Rolling.getRand());
                        newAbl.setQualitySelection(qualCat.get(0).getName());
                    }
                }
            }
            if (!abl.getBoostSelection().isEmpty()) {
                String[] options = abl.getBoostSelection().split(" or ");
                if (abl.getBoostSelection().contains("boost") || abl.getBoostSelection().contains("physical or energy")) {
                    if (Rolling.rollMe(2) == 1) {
                        newAbl.setBoostSelection(options[0]);
                    } else {
                        newAbl.setBoostSelection(options[1]);
                    }
                } else if (abl.getBoostSelection().contains("physical category or mental category")) {
                    List<Attribute> powOpts = new ArrayList<>(PowersCollection.getPowersByCat(options[0]));
                    powOpts.addAll(PowersCollection.getPowersByCat(options[1]));
                    powOpts.retainAll(availablePowers);
                    Collections.shuffle(powOpts, Rolling.getRand());
                    newAbl.setBoostSelection(powOpts.get(0).getName());
                } else {
                    System.out.println("Unknown selection type: boost selection of Utils");
                }
            }

            if (((!abl.getQualitySelection().isEmpty() && newAbl.getQualitySelection().isEmpty())
                    || (!abl.getPowerSelection().isEmpty() && newAbl.getPowerSelection().isEmpty())) && !abl.getPowerSelection().equals("any") && !abl.getQualitySelection().equals("any")) {
                System.out.println("parse ability failed to parse");
            }
        } catch (Exception e) {
            if (!abl.getName().equals("cosmic ray absorption") && !abl.getPowerSelection().equals("elemental/energy category") && !abl.getName().equals("resurrection")) {
                System.out.println(e.toString());
                e.printStackTrace();
            }
            return null;
        }
        return newAbl;
    }

    public List<Ability> loopForGoodAbilities(int choices, List<Attribute> heroPowers, List<Attribute> heroQualities, List<Ability> availAbilities, boolean allowDupePowsQuals) {
        List<Ability> availAbl = new ArrayList<>(availAbilities);
        Collections.shuffle(availAbl, Rolling.getRand());
        List<Attribute> remainingQuals = new ArrayList<>(heroQualities);
        List<Attribute> remainingPows = new ArrayList<>(heroPowers);

        List<Ability> retAbl = new ArrayList<>();
        for (int i = 0; i < choices; i++) {
            Ability addMe;
            while (true) {
                addMe = parseAbility(availAbl.get(i), remainingPows, remainingQuals);
                if (addMe != null) break;
                availAbl.remove(0);
            }
            retAbl.add(addMe);
            if (!allowDupePowsQuals) {
                Ability finalAddMe = addMe;
                if (!addMe.getPowerSelection().isEmpty()) {
                    remainingPows.removeIf(p -> p.getName().equals(finalAddMe.getPowerSelection()));
                }
                if (!addMe.getQualitySelection().isEmpty()) {
                    remainingQuals.removeIf(q -> q.getName().equals(finalAddMe.getQualitySelection()));
                }
            }
        }
        return retAbl;
    }

}
