package strongspearstrong.sentinelsautogen.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Rolling {
    private static Random rand = new Random();

    private static long seed;

    public static Random getRand(){
        return rand;
    }

    public static long getSeed() {
        return seed;
    }

    public static void setSeed(long seeding) {
        rand = new Random(seeding);
        seed = seeding;
    }

    public static void makeSeeded(){
        seed = rand.nextLong();
        rand = new Random(seed);
    }

    public static int rollMe(int sides) {
        return 1 + rand.nextInt(sides);
    }

    //takes a list of sides and outputs a list of results;
    public static List<Integer> rollListIntoList(List<Integer> dice) {
        List<Integer> res = new ArrayList<>();
        for (Integer i : dice) {
            res.add(rollMe(i));
        }
        return res;
    }

    public static int lazyCombinations(List<Integer> rolls) {
        if (rolls.size() == 2) {
            switch (rollMe(3)) {
                case 1:
                    return rolls.get(0);
                case 2:
                    return rolls.get(1);
                case 3:
                    return rolls.get(0) + rolls.get(1);
            }

        } else if (rolls.size() == 3) {
            switch (rollMe(6)) {
                case 1:
                    return rolls.get(0);
                case 2:
                    return rolls.get(1);
                case 3:
                    return rolls.get(2);
                case 4:
                    return rolls.get(0) + rolls.get(1);
                case 5:
                    return rolls.get(0) + rolls.get(2);
                case 6:
                    return rolls.get(1) + rolls.get(2);
            }
        }

        return 0;
    }
}
