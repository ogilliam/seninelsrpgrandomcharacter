package strongspearstrong.sentinelsautogen.utils;

public class AttributeTypes {
    public static final String QUALITY = "qualities";
    public static final String POWER = "powers";
}
