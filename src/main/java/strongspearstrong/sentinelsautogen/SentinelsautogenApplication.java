package strongspearstrong.sentinelsautogen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SentinelsautogenApplication {

    public static void main(String[] args) {
        SpringApplication.run(SentinelsautogenApplication.class, args);
    }

}
