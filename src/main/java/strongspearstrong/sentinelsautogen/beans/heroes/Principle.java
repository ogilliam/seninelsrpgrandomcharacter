package strongspearstrong.sentinelsautogen.beans.heroes;

public class Principle {
    private String name;
    private int pageNumber;

    public Principle(String name, int pageNumber) {
        this.name = name;
        this.pageNumber = pageNumber;
    }

    public Principle(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    @Override
    public String toString() {
        return name + " pg" + pageNumber;
    }
}
