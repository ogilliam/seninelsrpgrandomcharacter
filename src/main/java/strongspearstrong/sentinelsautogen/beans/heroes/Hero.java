package strongspearstrong.sentinelsautogen.beans.heroes;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.Characteristic;

import java.util.ArrayList;
import java.util.List;

public class Hero {
    private Characteristic background;
    private Characteristic powerSource;
    private Characteristic archetype;
    private Characteristic personality;
    private List<Principle> principles = new ArrayList<>();
    private List<Attribute> powers = new ArrayList<>();
    private List<Attribute> qualities = new ArrayList<>();
    private List<Ability> greenAbilities = new ArrayList<>();
    private List<Ability> yellowAbilities = new ArrayList<>();
    private List<Ability> redAbilities = new ArrayList<>();
    private Ability outAbility;
    private long seed;

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public List<Principle> getPrinciples() {
        return principles;
    }

    public void setPrinciples(List<Principle> principles) {
        this.principles = principles;
    }

    public void addPrinciple(Principle p) {
        this.principles.add(p);
    }

    public Characteristic getBackground() {
        return background;
    }

    public void setBackground(Characteristic background) {
        this.background = background;
    }

    public Characteristic getPowerSource() {
        return powerSource;
    }

    public void setPowerSource(Characteristic powerSource) {
        this.powerSource = powerSource;
    }

    public Characteristic getArchetype() {
        return archetype;
    }

    public void setArchetype(Characteristic archetype) {
        this.archetype = archetype;
    }

    public Characteristic getPersonality() {
        return personality;
    }

    public void setPersonality(Characteristic personality) {
        this.personality = personality;
    }

    public Ability getOutAbility() {
        return this.outAbility;
    }

    public void setOutAbility(Ability outAbility) {
        this.outAbility = outAbility;
    }

    public void addGreenAbility(Ability abl) {
        this.greenAbilities.add(abl);
    }

    public void addYellowAbility(Ability abl) {
        this.yellowAbilities.add(abl);
    }

    public void addRedAbility(Ability abl) {
        this.redAbilities.add(abl);
    }

    public void addRedAbilities(List<Ability> abl) {
        this.redAbilities.addAll(abl);
    }

    public List<Attribute> getPowers() {
        return this.powers;
    }

    public void setPowers(List<Attribute> powers) {
        this.powers = powers;
    }

    public void addPower(Attribute pow) {
        this.powers.add(pow);
    }

    public void addPowers(List<Attribute> powers) {
        this.powers.addAll(powers);
    }

    public List<Attribute> getQualities() {
        return this.qualities;
    }

    public void setQualities(List<Attribute> qualities) {
        this.qualities = qualities;
    }

    public void addQuality(Attribute qual) {
        this.qualities.add(qual);
    }

    public void addQualities(List<Attribute> qual) {
        this.qualities.addAll(qual);
    }

    public List<Ability> getGreenAbilities() {
        return greenAbilities;
    }

    public void setGreenAbilities(List<Ability> greenAbilities) {
        this.greenAbilities = greenAbilities;
    }

    public List<Ability> getYellowAbilities() {
        return yellowAbilities;
    }

    public void setYellowAbilities(List<Ability> yellowAbilities) {
        this.yellowAbilities = yellowAbilities;
    }

    public List<Ability> getRedAbilities() {
        return redAbilities;
    }

    public void setRedAbilities(List<Ability> redAbilities) {
        this.redAbilities = redAbilities;
    }

    public void addGreenAbilities(List<Ability> abl) {
        this.greenAbilities.addAll(abl);
    }

    public void addYellowAbilities(List<Ability> abl) {
        this.yellowAbilities.addAll(abl);
    }

}
