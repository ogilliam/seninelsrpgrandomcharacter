package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.powersources;

import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.QualitiesCollection;
import strongspearstrong.sentinelsautogen.utils.Rolling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Mystical extends AbstractPowerSource {
    @Override
    public void extraSteps(Hero hero) {
        List<Attribute> info = new ArrayList<>(QualitiesCollection.getQualitiesByCat("information"));
        info.removeAll(hero.getQualities());
        Collections.shuffle(info, Rolling.getRand());
        hero.addQuality(new Attribute(info.get(0).getName(), info.get(0).getCategory(), 10));
    }
}
