package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.powersources;

import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.QualitiesCollection;
import strongspearstrong.sentinelsautogen.utils.Rolling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Genius extends AbstractPowerSource {

    @Override
    public void extraSteps(Hero hero) {
        List<Attribute> availQual = new ArrayList<>();
        availQual.addAll(QualitiesCollection.getQualitiesByCat("information"));
        availQual.addAll(QualitiesCollection.getQualitiesByCat("mental"));
        availQual.removeAll(hero.getQualities());
        Collections.shuffle(availQual, Rolling.getRand());
        hero.addQuality(new Attribute(availQual.get(0).getName(), availQual.get(0).getCategory(), 10));
    }
}
