package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.beans.Attribute;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CloseQuartersCombat extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        //assigns one die to one of the mandatory qualities
        hero.addPowers(assignXFromArchToAttributes(getRequiredQualities(), hero.getQualities(), useDice, 1));

        //assigns one to a remaining any power
        hero.addPowers(assignOneToAttributeList(getRemainingAnyPowers(), hero.getPowers(), useDice));

        //Assigns rest of dice to powers and qualities
        disperseRemainingDiceToAttributes(getRemainingAnyPowers(), getRemainingAnyQualities(), hero, useDice);

        //get one explicitly from cqc
        List<Ability> gainedGreenAbilities = getXAbilitiesDifferentAttributesFromArchetypeList(getGreenAbilities(), 1, new ArrayList<>(),
                new ArrayList<>(Collections.singletonList(new Attribute("close combat", "physical"))));

        List<Ability> availGreenAbl = new ArrayList<>(getGreenAbilities());
        availGreenAbl.removeAll(gainedGreenAbilities);
        //get one using a power
        gainedGreenAbilities.addAll(getXAbilitiesDifferentAttributesFromHero(availGreenAbl, 1,
                hero.getPowers(), new ArrayList<>()));
        availGreenAbl.removeAll(gainedGreenAbilities);

        //gain a third doing whatever
        gainedGreenAbilities.addAll(getXAbilitiesDifferentAttributesFromHero(availGreenAbl, 1,
                hero.getPowers(), hero.getQualities()));

        hero.addGreenAbilities(gainedGreenAbilities);
        //gain yellow using different qualities/pows from green
        List<Attribute> availPow = new ArrayList<>(hero.getPowers());
        List<Attribute> availQual = new ArrayList<>(hero.getQualities());
        for (Ability a : gainedGreenAbilities) {
            availPow.remove(new Attribute(a.getPowerSelection(), ""));
            availQual.remove(new Attribute(a.getQualitySelection(), ""));
        }

        hero.addYellowAbilities(getXAbilitiesDifferentAttributesFromHero(getGreenAbilities(), getYellowChoices(), availPow, availQual));


        return hero;
    }
}
