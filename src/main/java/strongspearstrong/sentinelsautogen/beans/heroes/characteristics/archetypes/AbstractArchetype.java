package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.beans.heroes.Principle;
import strongspearstrong.sentinelsautogen.utils.AttributeTypes;
import strongspearstrong.sentinelsautogen.utils.Rolling;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        defaultImpl = BasicArchetype.class,
        property = "name",
        visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Armored.class, name = "armored"),
        @JsonSubTypes.Type(value = Blaster.class, name = "blaster"),
        @JsonSubTypes.Type(value = CloseQuartersCombat.class, name = "close quarters combatant"),
        @JsonSubTypes.Type(value = Divided.class, name = "divided"),
        @JsonSubTypes.Type(value = ElementalManipulator.class, name = "elemental manipulator"),
        @JsonSubTypes.Type(value = Flyer.class, name = "flyer"),
        @JsonSubTypes.Type(value = FormChanger.class, name = "form-changer"),
        @JsonSubTypes.Type(value = Gadgeteer.class, name = "gadgeteer"),
        @JsonSubTypes.Type(value = Marksman.class, name = "marksman"),
        @JsonSubTypes.Type(value = MinionMaker.class, name = "minion-maker"),
        @JsonSubTypes.Type(value = Modular.class, name = "modular"),
        @JsonSubTypes.Type(value = PhysicalPowerhouse.class, name = "physical powerhouse"),
        @JsonSubTypes.Type(value = Psychic.class, name = "psychic"),
        @JsonSubTypes.Type(value = RealityShaper.class, name = "reality shaper"),
        @JsonSubTypes.Type(value = RobotCyborg.class, name = "robot/cyborg"),
        @JsonSubTypes.Type(value = Shadow.class, name = "shadow"),
        @JsonSubTypes.Type(value = Speedster.class, name = "speedster"),
        @JsonSubTypes.Type(value = Sorcerer.class, name = "sorcerer"),
        @JsonSubTypes.Type(value = Transporter.class, name = "transporter"),
        @JsonSubTypes.Type(value = WildCard.class, name = "wild card"),
})
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractArchetype {

    private int selectionNumber;
    private String name;
    private List<String> requiredPowers = new ArrayList<>();
    private List<String> requiredQualities = new ArrayList<>();
    private List<String> remainingAnyPowers = new ArrayList<>();
    private List<String> remainingAnyQualities = new ArrayList<>();
    private String principle;
    List<Ability> yellowAbilities = new ArrayList<>();
    List<Ability> greenAbilities;
    List<Ability> redAbilities = new ArrayList<>();
    List<Ability> secondaryGreenAbilities = new ArrayList<>();
    List<Ability> freeGreenAbilities = new ArrayList<>();
    List<Ability> freeRedAbilities = new ArrayList<>();
    List<Ability> freeYellowAbilities = new ArrayList<>();
    private int yellowChoices;
    private int greenChoices;
    private int redChoices;
    private int secondaryGreenChoices;
    private int pageNumber;


    public Hero archetypeBuilder(Hero myHero, List<Integer> useDice) {
        Collections.shuffle(useDice, Rolling.getRand());
        if (myHero.getPowerSource().getName().equals("training") && !myHero.getArchetype().getName().equals("divided") && !myHero.getArchetype().getName().equals("modular")) {
            myHero.addQuality(trainingQuality(myHero.getQualities()));
        }
        myHero = customStepList(myHero, useDice);

        if(!getPrinciple().isEmpty()){
            myHero.addPrinciple(new Utils().getPrinciple(getPrinciple()));
        }


        return myHero;
    }

    //each archetype has its own set of steps to use, which much be defined on a per archetype basis
    protected abstract Hero customStepList(Hero hero, List<Integer> useDice);


    //checks if the training background was take, and if so, adds a quality
    private Attribute trainingQuality(List<Attribute> heroQuals) {
        Utils utils = new Utils();
        List<Attribute> availableQualities = new ArrayList<>(utils.attributesExtraction(getRequiredQualities(), AttributeTypes.QUALITY));
        availableQualities.addAll(utils.attributesExtraction(getRemainingAnyQualities(), AttributeTypes.QUALITY));
        availableQualities.removeAll(heroQuals);
        Collections.shuffle(availableQualities);
        return new Attribute(availableQualities.get(0).getName(), availableQualities.get(0).getCategory(), 8);
    }

    //assigns one die to one of the mandatory powers, if not have yet
    protected List<Attribute> assignOneToAttributeList(List<String> availableAttributes, List<Attribute> heroAttributes, List<Integer> die) {
        //Puts through blank hero list so full categories can be checked
        List<Attribute> availAttributes = new Utils().getNewAvailableAttributes(heroAttributes, availableAttributes, AttributeTypes.POWER);

        //if the hero does not currently hold any of the available attributes, gives one
        Collections.shuffle(availAttributes, Rolling.getRand());
        List<Attribute> retAtt = new ArrayList<>();
        if(availAttributes.size() > 0){
            retAtt.add(new Attribute(availAttributes.get(0).getName(), availAttributes.get(0).getName(), die.get(0)));
            die.remove(0);
        }
        return retAtt;
    }

    protected List<Attribute> assignXFromArchToAttributes(List<String> availableAttributes, List<Attribute> heroAttributes, List<Integer> die, int assign) {
        List<Attribute> availAttributes = new Utils().attributesExtraction(availableAttributes, AttributeTypes.POWER);
        Collections.shuffle(availAttributes, Rolling.getRand());
        List<Attribute> heroHasAvailable = new ArrayList<>(heroAttributes);
        heroHasAvailable.retainAll(availAttributes);
        int heroHolds = heroHasAvailable.size();
        List<Attribute> retAtt = new ArrayList<>();

        if (availAttributes.size() > 0) {
            while (heroHolds < assign) {
                retAtt.add(new Attribute(availAttributes.get(0).getName(), availAttributes.get(0).getCategory(), die.get(0)));
                die.remove(0);
                availAttributes.remove(0);
                heroHolds++;
            }
        }

        return retAtt;
    }

    protected void disperseRemainingDiceToAttributes(List<String> newPowers, List<String> newQualities, Hero hero, List<Integer> dice) {
        Utils utils = new Utils();
        List<Attribute> availPows = utils.getNewAvailableAttributes(hero.getPowers(), newPowers, AttributeTypes.POWER);
        List<Attribute> availQuals = utils.getNewAvailableAttributes(hero.getQualities(), newQualities, AttributeTypes.QUALITY);

        for (Integer d : dice) {
            int roll = Rolling.rollMe(availPows.size() + availQuals.size());
            roll--;
            if (roll < availPows.size()) {
                hero.addPower(new Attribute(availPows.get(roll).getName(), availPows.get(roll).getCategory(), d));
            } else {
                hero.addQuality(new Attribute(availQuals.get(roll - availPows.size()).getName(), availQuals.get(roll - availPows.size()).getCategory(), d));
            }
        }
    }

    protected List<Ability> getXAbilitiesDifferentAttributesFromArchetypeList(List<Ability> abilities, int choices, List<Attribute> heroPowers, List<Attribute> heroQualities) {
        Utils utils = new Utils();
        List<String> powersMasterList = new ArrayList<>(getRequiredPowers());
        powersMasterList.addAll(getRemainingAnyPowers());
        List<String> qualityMasterList = new ArrayList<>(getRequiredQualities());
        qualityMasterList.addAll(getRemainingAnyQualities());
        List<Attribute> availablePowers = utils.getUseAvailableAttributes(heroPowers, powersMasterList, AttributeTypes.POWER);
        List<Attribute> availableQualities = utils.getUseAvailableAttributes(heroQualities, qualityMasterList, AttributeTypes.QUALITY);

        return utils.loopForGoodAbilities(choices, availablePowers, availableQualities, abilities, false);
    }

    protected List<Ability> getXAbilitiesDifferentAttributesFromHero(List<Ability> abilities, int choices, List<Attribute> heroPowers, List<Attribute> heroQualities) {
        return new Utils().loopForGoodAbilities(choices, new ArrayList<>(heroPowers), new ArrayList<>(heroQualities), abilities, false);
    }

    protected List<Ability> getXAbilitiesAnyAttributesFromHero(List<Ability> abilities, int choices, List<Attribute> heroPowers, List<Attribute> heroQualities) {
        return new Utils().loopForGoodAbilities(choices, new ArrayList<>(heroPowers), new ArrayList<>(heroQualities), abilities, true);
    }

    public Principle makePrinciple() {
        return new Utils().getPrinciple(principle);
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getSelectionNumber() {
        return selectionNumber;
    }

    public void setSelectionNumber(int selectionNumber) {
        this.selectionNumber = selectionNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getRequiredPowers() {
        return requiredPowers;
    }

    public void setRequiredPowers(List<String> requiredPowers) {
        this.requiredPowers = requiredPowers;
    }

    public List<String> getRequiredQualities() {
        return requiredQualities;
    }

    public void setRequiredQualities(List<String> requiredQualities) {
        this.requiredQualities = requiredQualities;
    }


    public List<String> getRemainingAnyPowers() {
        return remainingAnyPowers;
    }

    public void setRemainingAnyPowers(List<String> remainingAnyPowers) {
        this.remainingAnyPowers = remainingAnyPowers;
    }

    public List<String> getRemainingAnyQualities() {
        return remainingAnyQualities;
    }

    public void setRemainingAnyQualities(List<String> remainingAnyQualities) {
        this.remainingAnyQualities = remainingAnyQualities;
    }

    public String getPrinciple() {
        return principle;
    }

    public void setPrinciple(String principle) {
        this.principle = principle;
    }

    public int getRedChoices() {
        return redChoices;
    }

    public void setRedChoices(int freeRedChoicess) {
        this.redChoices = freeRedChoicess;
    }

    public List<Ability> getFreeYellowAbilities() {
        return new ArrayList<>(freeYellowAbilities);
    }

    public void setFreeYellowAbilities(List<Ability> freeYellowAbilities) {
        this.freeYellowAbilities = freeYellowAbilities;
    }

    public List<Ability> getFreeRedAbilities() {
        return freeRedAbilities;
    }

    public void setFreeRedAbilities(List<Ability> freeRedAbilities) {
        this.freeRedAbilities = freeRedAbilities;
    }

    public List<Ability> getSecondaryGreenAbilities() {
        return new ArrayList<>(secondaryGreenAbilities);
    }

    public void setSecondaryGreenAbilities(List<Ability> secondaryGreenAbilities) {
        this.secondaryGreenAbilities = secondaryGreenAbilities;
    }

    public int getSecondaryGreenChoices() {
        return secondaryGreenChoices;
    }

    public void setSecondaryGreenChoices(int secondaryGreenChoices) {
        this.secondaryGreenChoices = secondaryGreenChoices;
    }

    public List<Ability> getYellowAbilities() {
        return new ArrayList<>(yellowAbilities);
    }

    public List<Ability> getFreeGreenAbilities() {
        return new ArrayList<>(freeGreenAbilities);
    }

    public void setFreeGreenAbilities(List<Ability> freeGreenAbilities) {
        this.freeGreenAbilities = freeGreenAbilities;
    }

    public void setYellowAbilities(List<Ability> yellowAbilities) {
        this.yellowAbilities = yellowAbilities;
    }

    public List<Ability> getGreenAbilities() {
        return new ArrayList<>(greenAbilities);
    }

    public void setGreenAbilities(List<Ability> greenAbilities) {
        this.greenAbilities = greenAbilities;
    }

    public int getYellowChoices() {
        return yellowChoices;
    }

    public void setYellowChoices(int yellowChoices) {
        this.yellowChoices = yellowChoices;
    }

    public int getGreenChoices() {
        return greenChoices;
    }

    public void setGreenChoices(int greenChoices) {
        this.greenChoices = greenChoices;
    }

    public List<Ability> getRedAbilities() {
        return redAbilities;
    }

    public void setRedAbilities(List<Ability> redAbilities) {
        this.redAbilities = redAbilities;
    }
}
