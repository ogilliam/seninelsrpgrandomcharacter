package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.heroes.Hero;

import java.util.List;

public class RealityShaper extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        //assigns one to a remaining any power
        hero.addPowers(assignOneToAttributeList(getRemainingAnyPowers(), hero.getPowers(), useDice));

        disperseRemainingDiceToAttributes(getRemainingAnyPowers(), getRemainingAnyQualities(), hero, useDice);

        hero.addGreenAbilities(getXAbilitiesDifferentAttributesFromHero(getGreenAbilities(), getGreenChoices(), hero.getPowers(), hero.getQualities()));
        hero.addYellowAbilities(getXAbilitiesDifferentAttributesFromHero(getYellowAbilities(), getYellowChoices(), hero.getPowers(), hero.getQualities()));

        return hero;
    }
}
