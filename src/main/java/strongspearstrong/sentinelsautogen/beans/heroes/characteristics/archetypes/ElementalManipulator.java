package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.collections.PowersCollection;
import strongspearstrong.sentinelsautogen.utils.AttributeTypes;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class ElementalManipulator extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        //assigns one die to one of the mandatory powers
        hero.addPowers(assignXFromArchToAttributes(getRequiredPowers(), hero.getPowers(), useDice, 1));

        //assigns one to a remaining any power
        hero.addPowers(assignOneToAttributeList(getRemainingAnyPowers(), hero.getPowers(), useDice));

        //Only obtains qualities after
        disperseRemainingDiceToAttributes(new ArrayList<>(), getRemainingAnyQualities(), hero, useDice);

        List<Attribute> availablePowers = new ArrayList<>(new Utils().getUseAvailableAttributes(hero.getPowers()
                , PowersCollection.getPowersByCat("elemental/energy").stream().map(Attribute::getName).collect(toList())
                , AttributeTypes.POWER));

        hero.addGreenAbilities(getXAbilitiesAnyAttributesFromHero(getGreenAbilities(), getGreenChoices(), availablePowers, new ArrayList<>()));
        hero.addYellowAbilities(getXAbilitiesAnyAttributesFromHero(getYellowAbilities(), getYellowChoices(), availablePowers, new ArrayList<>()));

        return hero;
    }
}
