package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.beans.Attribute;

import java.util.ArrayList;
import java.util.List;

public class Flyer extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        //assigns one die to one of the mandatory powers
        hero.addPowers(assignXFromArchToAttributes(getRequiredPowers(), hero.getPowers(), useDice, 1));

        //assigns one to a remaining any power
        hero.addPowers(assignOneToAttributeList(getRemainingAnyPowers(), hero.getPowers(), useDice));

        disperseRemainingDiceToAttributes(getRemainingAnyPowers(), getRemainingAnyQualities(), hero, useDice);

        //get one explicitly from flyer
        List<Attribute> initialAttri = new ArrayList<>();
        for (String s : getRequiredPowers()) {
            initialAttri.add(new Attribute(s, ""));
        }
        initialAttri.retainAll(hero.getPowers());
        List<Ability> gainedGreenAbilities = getXAbilitiesDifferentAttributesFromArchetypeList(getGreenAbilities(), 1, initialAttri, hero.getQualities());

        List<Ability> availGreenAbl = new ArrayList<>(getGreenAbilities());
        availGreenAbl.removeAll(gainedGreenAbilities);
        //get one using a quality
        gainedGreenAbilities.addAll(getXAbilitiesDifferentAttributesFromHero(availGreenAbl, getGreenChoices() - 1,
                hero.getPowers(), hero.getQualities()));

        hero.addGreenAbilities(gainedGreenAbilities);

        hero.addYellowAbilities(getXAbilitiesDifferentAttributesFromHero(getGreenAbilities(), getYellowChoices(), hero.getPowers(), hero.getQualities()));

        return hero;
    }
}
