package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;

import java.util.ArrayList;
import java.util.List;

public class Armored extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        //assigns one to a remaining any power
        hero.addPowers(assignOneToAttributeList(getRemainingAnyPowers(), hero.getPowers(), useDice));

        disperseRemainingDiceToAttributes(getRemainingAnyPowers(), getRemainingAnyQualities(), hero, useDice);

        //gets the one free green  ability
        hero.addGreenAbility(new Ability(getFreeGreenAbilities().get(0).getName(), "", "", ""));

        //gains two with different powers, then gains one with any
        List<Ability> gainedGreenAbilities = getXAbilitiesDifferentAttributesFromHero(getGreenAbilities(), 2, hero.getPowers(), hero.getQualities());
        List<Ability> availGA = new ArrayList<>(getGreenAbilities());
        availGA.removeAll(gainedGreenAbilities);

        hero.addGreenAbilities(getXAbilitiesDifferentAttributesFromHero(availGA, 1, hero.getPowers(), hero.getQualities()));

        hero.addGreenAbilities(gainedGreenAbilities);

        return hero;
    }
}
