package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.personalities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.Characteristic;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.heroes.RedAbilityCollection;
import strongspearstrong.sentinelsautogen.utils.Rolling;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@JsonDeserialize(as = BasicPersonality.class)
public abstract class AbstractPersonality {
    private int selectionNumber;
    private String name;
    private Ability outAbility;
    private int pageNumber;

    public Hero personalityBuilder(Hero hero) {
        hero.setPersonality(new Characteristic(name, pageNumber));
        hero.setOutAbility(makeOutAbility(hero.getPowers(), hero.getQualities()));
        hero.addRedAbilities(makeRedAbilities(hero.getPowers(), hero.getQualities(), hero.getArchetype().getName()));
        return hero;
    }

    private Ability makeOutAbility(List<Attribute> powers, List<Attribute> qualities) {
        return new Utils().parseAbility(outAbility, powers, qualities);
    }

    protected List<Ability> makeRedAbilities(List<Attribute> heroPowerList, List<Attribute> heroQualList, String archName) {
        Set<String> availableCats = heroPowerList.stream().map(Attribute::getCategory).collect(toSet());
        availableCats.addAll(heroQualList.stream().map(Attribute::getCategory).collect(toSet()));

        List<Ability> availableRedAbls = new ArrayList<>();
        for (String s : availableCats) {
            availableRedAbls.addAll(RedAbilityCollection.getRedAbilitiesByCategory(s));
        }
        //accounting for divided and modular
        if(archName.contains("minion-maker")){
            availableRedAbls.addAll(RedAbilityCollection.getRedAbilitiesByCategory("minion-maker"));
        }


        Collections.shuffle(availableRedAbls, Rolling.getRand());

        if (availableCats.contains("hallmark")) {
            if (!heroPowerList.contains(new Attribute("signature weaponry", "hallmark"))) {
                availableRedAbls.removeIf(a -> a.getPowerSelection().equals("signature weaponry"));
            }
            if (!heroPowerList.contains(new Attribute("signature vehicle", "hallmark"))) {
                availableRedAbls.removeIf(a -> a.getPowerSelection().equals("signature vehicle"));
            }
        }

        return new Utils().loopForGoodAbilities(2, heroPowerList,heroQualList ,availableRedAbls, true);
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getSelectionNumber() {
        return selectionNumber;
    }

    public void setSelectionNumber(int selectionNumber) {
        this.selectionNumber = selectionNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ability getOutAbility() {
        return outAbility;
    }

    public void setOutAbility(Ability outAbility) {
        this.outAbility = outAbility;
    }
}
