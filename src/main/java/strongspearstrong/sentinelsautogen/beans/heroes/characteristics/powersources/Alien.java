package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.powersources;

import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.utils.AttributeTypes;
import strongspearstrong.sentinelsautogen.utils.Rolling;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Alien extends AbstractPowerSource {
    @Override
    public void extraSteps(Hero hero) {
        List<Attribute> applicablePowers = hero.getPowers().stream().filter(p -> p.getDieRating() == 6).collect(toList());
        List<Attribute> applicableQualities = hero.getQualities().stream().filter(q -> q.getDieRating() == 6).collect(toList());

        if (applicablePowers.size() + applicableQualities.size() > 0) {
            int roll = Rolling.rollMe(applicablePowers.size() + applicableQualities.size());
            if (roll <= applicablePowers.size()) {
                applicablePowers.get(roll -1 ).setDieRating(8);
            } else {
                applicableQualities.get(roll - applicablePowers.size() - 1).setDieRating(8);
            }

        } else {
            List<Attribute> availPowers = new ArrayList<>(new Utils().getNewAvailableAttributes(hero.getPowers(), getPowers(), AttributeTypes.POWER));
            Collections.shuffle(availPowers, Rolling.getRand());
            hero.addPower(new Attribute(availPowers.get(0).getName(), availPowers.get(0).getCategory(), 6));
        }
    }
}
