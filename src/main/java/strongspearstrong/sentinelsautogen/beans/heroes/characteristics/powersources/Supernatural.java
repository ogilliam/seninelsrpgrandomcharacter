package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.powersources;

import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.PowersCollection;
import strongspearstrong.sentinelsautogen.utils.AttributeTypes;
import strongspearstrong.sentinelsautogen.utils.Rolling;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Supernatural extends AbstractPowerSource {
    @Override
    public void extraSteps(Hero hero) {
        List<Attribute> availablePowers = new ArrayList<>(PowersCollection.getPowers());
        availablePowers.removeAll(hero.getPowers());
        availablePowers.removeAll(new Utils().attributesExtraction(getPowers(), AttributeTypes.POWER));

        Collections.shuffle(availablePowers, Rolling.getRand());

        hero.addPower(new Attribute(availablePowers.get(0).getName(), availablePowers.get(0).getCategory(), 10));
    }
}
