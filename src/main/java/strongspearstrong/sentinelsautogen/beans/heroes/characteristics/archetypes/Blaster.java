package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.heroes.Hero;

import java.util.ArrayList;
import java.util.List;

public class Blaster extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {

        //assigns one die to one of the mandatory powers
        hero.addPowers(assignXFromArchToAttributes(getRequiredPowers(), hero.getPowers(), useDice, 1));

        //assigns one to a remaining any power
        hero.addPowers(assignOneToAttributeList(getRemainingAnyPowers(), hero.getPowers(), useDice));

        //Only obtains qualities after
        disperseRemainingDiceToAttributes(new ArrayList<>(), getRemainingAnyQualities(), hero, useDice);

        //no abilities ask for qualities, so not sending them in
        hero.addGreenAbilities(getXAbilitiesDifferentAttributesFromArchetypeList(getGreenAbilities(), getGreenChoices(), hero.getPowers(), new ArrayList<>()));
        hero.addYellowAbilities(getXAbilitiesDifferentAttributesFromHero(getYellowAbilities(), getYellowChoices(), hero.getPowers(), new ArrayList<>()));

        return hero;
    }
}
