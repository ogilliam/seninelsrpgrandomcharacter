package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.Characteristic;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.heroes.ArchetypesCollection;
import strongspearstrong.sentinelsautogen.collections.heroes.PowerSourceCollection;
import strongspearstrong.sentinelsautogen.collections.PowersCollection;
import strongspearstrong.sentinelsautogen.utils.AttributeTypes;
import strongspearstrong.sentinelsautogen.utils.Rolling;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Modular extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {

        List<Integer> archDice = new ArrayList<>(PowerSourceCollection.getPowerSourceByName(hero.getPowerSource().getName()).getNextDice());
        int res;
        List<Integer> roll = Rolling.rollListIntoList(archDice);
        do {
            res = Rolling.lazyCombinations(roll);
        } while (res == 19 || res == 20);
        AbstractArchetype newArch = ArchetypesCollection.getArchetypeByRoll(res);
        hero.getArchetype().setName("modular - " + newArch.getName());
        Hero newHero = new Hero();
        newHero.setPowerSource(hero.getPowerSource());
        newHero.setArchetype(new Characteristic(newArch.getName(), newArch.getPageNumber()));
        newHero = newArch.archetypeBuilder(newHero, useDice);

        hero.addPowers(newHero.getPowers());
        hero.addQualities(newHero.getQualities());

        if (hero.getPowers().size() < 4) {
            int newPowCount = 0;
            List<Attribute> powerOptions = new Utils().getNewAvailableAttributes(hero.getPowers(),
                    PowersCollection.getPowers().stream().map(Attribute::getName).collect(Collectors.toList()),
                    AttributeTypes.POWER);
            Collections.shuffle(powerOptions, Rolling.getRand());
            while (hero.getPowers().size() < 4 || newPowCount > 1) {
                hero.addPower(new Attribute(powerOptions.get(0).getName(), powerOptions.get(0).getCategory(), 6));
                newPowCount++;
                powerOptions.remove(0);
            }
        }

        hero.addGreenAbilities(getXAbilitiesDifferentAttributesFromHero(getFreeGreenAbilities(), 1, hero.getPowers(), hero.getQualities()));
        hero.addYellowAbilities(getXAbilitiesDifferentAttributesFromHero(getFreeYellowAbilities(), 1, hero.getPowers(), hero.getQualities()));
        hero.addRedAbilities(getXAbilitiesDifferentAttributesFromHero(getFreeRedAbilities(), 1, hero.getPowers(), hero.getQualities()));

        hero.addGreenAbilities(getXAbilitiesDifferentAttributesFromHero(getGreenAbilities(), getGreenChoices(), hero.getPowers(), hero.getQualities()));
        hero.addYellowAbilities(getXAbilitiesAnyAttributesFromHero(getYellowAbilities(), getYellowChoices(), hero.getPowers(), hero.getQualities()));
        hero.addRedAbilities(getXAbilitiesDifferentAttributesFromHero(getRedAbilities(), getRedChoices(), hero.getPowers(), hero.getQualities()));

        return hero;
    }
}
