package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.powersources;

import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.PowersCollection;
import strongspearstrong.sentinelsautogen.utils.Rolling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Multiverse extends AbstractPowerSource {
    @Override
    public void extraSteps(Hero hero) {
        List<Attribute> availPowers = new ArrayList<>(PowersCollection.getPowers());
        availPowers.removeAll(hero.getPowers());
        Collections.shuffle(availPowers, Rolling.getRand());
        hero.addPower(new Attribute(availPowers.get(0).getName(), availPowers.get(0).getCategory(), 6));
    }
}
