package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.utils.AttributeTypes;
import strongspearstrong.sentinelsautogen.utils.Rolling;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RobotCyborg extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        //assigns one to a remaining any power
        hero.addPowers(assignOneToAttributeList(getRemainingAnyPowers(), hero.getPowers(), useDice));

        disperseRemainingDiceToAttributes(getRemainingAnyPowers(), getRemainingAnyQualities(), hero, useDice);

        //gets a list of new tech power to obtain
        List<Attribute> newTech = new Utils().getNewAvailableAttributes(hero.getPowers()
                , new ArrayList<>(Collections.singletonList("technological category"))
                , AttributeTypes.POWER);
        Collections.shuffle(newTech, Rolling.getRand());

        hero.addPower(new Attribute(newTech.get(0).getName(), newTech.get(0).getCategory(), 10));

        hero.addGreenAbilities(getXAbilitiesDifferentAttributesFromHero(getGreenAbilities(), getGreenChoices(), hero.getPowers(), hero.getQualities()));
        hero.addYellowAbilities(getXAbilitiesDifferentAttributesFromHero(getGreenAbilities(), getYellowChoices(), hero.getPowers(), hero.getQualities()));

        return hero;
    }
}
