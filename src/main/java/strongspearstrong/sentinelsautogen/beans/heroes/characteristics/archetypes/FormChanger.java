package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;

import java.util.List;

public class FormChanger extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        //assigns one die to one of the mandatory powers
        hero.addPowers(assignXFromArchToAttributes(getRequiredPowers(), hero.getPowers(), useDice, 1));

        //assigns one to a remaining any power
        hero.addPowers(assignOneToAttributeList(getRemainingAnyPowers(), hero.getPowers(), useDice));

        disperseRemainingDiceToAttributes(getRemainingAnyPowers(), getRemainingAnyQualities(), hero, useDice);

        //gets change forms
        hero.addGreenAbilities(getXAbilitiesDifferentAttributesFromHero(getFreeGreenAbilities(),getFreeGreenAbilities().size(), hero.getPowers(), hero.getQualities()));

        //gets one of other shifter forms
        hero.addGreenAbilities(getXAbilitiesDifferentAttributesFromHero(getSecondaryGreenAbilities(),getSecondaryGreenChoices(), hero.getPowers(), hero.getQualities()));

        //select two other green shifter forms
        hero.addGreenAbilities(getXAbilitiesAnyAttributesFromHero(getGreenAbilities(),getGreenChoices(), hero.getPowers(), hero.getQualities()));

        //select one yellow
        hero.addYellowAbilities(getXAbilitiesDifferentAttributesFromHero(getYellowAbilities(),getYellowChoices(), hero.getPowers(), hero.getQualities()));

        //gets a single red that takes no powers
        hero.addRedAbility(new Ability(getFreeRedAbilities().get(0).getName(), "", "", ""));

        return hero;
    }
}
