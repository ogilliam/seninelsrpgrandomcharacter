package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.backgrounds;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.QualitiesCollection;
import strongspearstrong.sentinelsautogen.utils.AttributeTypes;
import strongspearstrong.sentinelsautogen.utils.Rolling;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@JsonDeserialize(as = BasicBackground.class)
public abstract class AbstractBackground {
    private int selectionNumber;
    private String name;
    private List<String> qualities;
    private String principle;
    private List<Integer> useDice;
    private List<Integer> nextDice;
    private List<String> mandatorySelection;
    private int pageNumber;

    public Hero backgroundBuilder(Hero hero) {
        hero.addQualities(makeQualities(hero.getQualities()));
        hero.addPrinciple(new Utils().getPrinciple(principle));
        return hero;
    }

    public List<Attribute> makeQualities(List<Attribute> heroAtribs) {
        List<Integer> usingDice = new ArrayList<>(useDice);
        Collections.shuffle(usingDice, Rolling.getRand());
        List<Attribute> newQualities = new ArrayList<>();

        for (String mand : mandatorySelection) {
            newQualities.add(new Attribute(mand, QualitiesCollection.getQualityByName(mand).getCategory(), usingDice.get(0)));
            usingDice.remove(0);
        }

        List<Attribute> availableQualities = new Utils().getNewAvailableAttributes(heroAtribs, qualities, AttributeTypes.QUALITY);

        for (Integer d : usingDice) {
            newQualities.add(new Attribute(availableQualities.get(0).getName(), availableQualities.get(0).getCategory(), d));
            availableQualities.remove(0);
        }

        return newQualities;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getSelectionNumber() {
        return selectionNumber;
    }

    public void setSelectionNumber(int selectionNumber) {
        this.selectionNumber = selectionNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getQualities() {
        return qualities;
    }

    public void setQualities(List<String> qualities) {
        this.qualities = qualities;
    }

    public String getPrinciple() {
        return principle;
    }

    public void setPrinciple(String principle) {
        this.principle = principle;
    }

    public List<Integer> getUseDice() {
        return useDice;
    }

    public void setUseDice(List<Integer> useDice) {
        this.useDice = useDice;
    }

    public List<Integer> getNextDice() {
        return nextDice;
    }

    public void setNextDice(List<Integer> nextDice) {
        this.nextDice = nextDice;
    }

    public List<String> getMandatorySelection() {
        return mandatorySelection;
    }

    public void setMandatorySelection(List<String> mandatorySelection) {
        this.mandatorySelection = mandatorySelection;
    }
}
