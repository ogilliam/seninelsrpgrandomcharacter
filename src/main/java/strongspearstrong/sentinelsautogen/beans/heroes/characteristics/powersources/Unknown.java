package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.powersources;

import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.QualitiesCollection;
import strongspearstrong.sentinelsautogen.utils.Rolling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Unknown extends AbstractPowerSource {
    @Override
    public void extraSteps(Hero hero) {
        List<Attribute> soc = new ArrayList<>(QualitiesCollection.getQualitiesByCat("social"));
        soc.removeAll(hero.getQualities());
        Collections.shuffle(soc, Rolling.getRand());
        hero.addQuality(new Attribute(soc.get(0).getName(), soc.get(0).getCategory(), 8));
    }
}
