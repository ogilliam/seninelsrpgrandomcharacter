package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.PowersCollection;

import java.util.ArrayList;
import java.util.List;

public class PhysicalPowerhouse extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        //assigns one die to one of the mandatory powers
        hero.addPowers(assignXFromArchToAttributes(getRequiredPowers(), hero.getPowers(), useDice, 1));

        //assigns one to a remaining any power
        hero.addPowers(assignOneToAttributeList(getRemainingAnyPowers(), hero.getPowers(), useDice));

        //Only
        disperseRemainingDiceToAttributes(getRemainingAnyPowers(), getRemainingAnyQualities(), hero, useDice);

        //can only get yellow from physical powerhouse list, and on ones not used in green
        List<Attribute> allowedGreenPowers = new ArrayList<>(hero.getPowers());
        allowedGreenPowers.retainAll(PowersCollection.getPowersByCat("physical"));
        List<Attribute> allowedGreenQualities = new ArrayList<>(hero.getQualities());
        List<Ability> gainedGreenAbilities = getXAbilitiesDifferentAttributesFromArchetypeList(getGreenAbilities(), getGreenChoices(), hero.getPowers(), hero.getQualities());
        for(Ability a : gainedGreenAbilities){
            allowedGreenPowers.remove(new Attribute(a.getPowerSelection(), ""));
            allowedGreenQualities.remove(new Attribute(a.getQualitySelection(), ""));
        }

        hero.addGreenAbilities(gainedGreenAbilities);
        hero.addYellowAbilities(getXAbilitiesDifferentAttributesFromHero(getGreenAbilities(), getYellowChoices(), allowedGreenPowers, allowedGreenQualities));

        return hero;
    }
}
