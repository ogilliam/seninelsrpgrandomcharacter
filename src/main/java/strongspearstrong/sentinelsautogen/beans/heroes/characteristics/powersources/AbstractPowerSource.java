package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.powersources;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.PowersCollection;
import strongspearstrong.sentinelsautogen.utils.AttributeTypes;
import strongspearstrong.sentinelsautogen.utils.Rolling;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        defaultImpl = BasicPowerSource.class,
        property = "name",
        visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Alien.class, name = "alien"),
        @JsonSubTypes.Type(value = Cosmos.class, name = "cosmos"),
        @JsonSubTypes.Type(value = Genius.class, name = "genius"),
        @JsonSubTypes.Type(value = Multiverse.class, name = "the multiverse"),
        @JsonSubTypes.Type(value = Mystical.class, name = "mystical"),
        @JsonSubTypes.Type(value = Supernatural.class, name = "supernatural"),
        @JsonSubTypes.Type(value = Unknown.class, name = "unknown"),
})
public abstract class AbstractPowerSource {
    private int selectionNumber;
    private String name;
    private List<String> powers;
    private List<Integer> nextDice;
    private List<String> requiredPowers = new ArrayList<>();
    List<Ability> yellowAbilities;
    List<Ability> greenAbilities = new ArrayList<>();
    private int yellowChoices;
    private int greenChoices;
    private int pageNumber;

    public Hero powerSourceBuilder(Hero myHero, List<Integer> useDice) {
        myHero.addPowers(makePowers(useDice));
        myHero.addGreenAbilities(makeGreenAbilities(myHero.getPowers(), myHero.getQualities()));
        myHero.addYellowAbilities(makeYellowAbilities(myHero.getPowers(), myHero.getQualities()));
        extraSteps(myHero);
        return myHero;
    }

    protected abstract void extraSteps(Hero hero);

    private List<Attribute> makePowers(List<Integer> useDice) {
        List<Attribute> newPows = new ArrayList<>();
        List<Integer> dice = new ArrayList<>(useDice);
        Collections.shuffle(dice, Rolling.getRand());
        for (String s : requiredPowers) {
            newPows.add(new Attribute(s, PowersCollection.getPowerByName(s).getCategory(),dice.get(0)));
            dice.remove(0);
        }

        List<Attribute> powOptions = new ArrayList<>(new Utils().getNewAvailableAttributes(newPows, powers, AttributeTypes.POWER));
        Collections.shuffle(powOptions, Rolling.getRand());
        for (Integer d : dice) {
            newPows.add(new Attribute(powOptions.get(0).getName(), powOptions.get(0).getCategory(), d));
            powOptions.remove(0);
        }

        return newPows;
    }

    private List<Ability> makeGreenAbilities(List<Attribute> heroPowers, List<Attribute> heroQualities) {
        return new Utils().loopForGoodAbilities(greenChoices, heroPowers, heroQualities, getGreenAbilities(), true);
    }

    private List<Ability> makeYellowAbilities(List<Attribute> heroPowers, List<Attribute> heroQualities) {
        Utils utils = new Utils();
        List<Ability> yellowAbls = new ArrayList<>();
        List<Ability> psYellow = getYellowAbilities();
        Collections.shuffle(psYellow, Rolling.getRand());
        //requires different powers/qualities
        List<Attribute> allowedPowers = new ArrayList<>(heroPowers);
        List<Attribute> allowedQuals = new ArrayList<>(heroQualities);


        for (int i = 0; i < yellowChoices; i++) {
            Ability addMe;
            while(true){
               addMe = utils.parseAbility(psYellow.get(i), allowedPowers, allowedQuals);
               if(addMe != null) break;
               psYellow.remove(0);
            }

            if (!addMe.getPowerSelection().isEmpty()) {
                Ability finalAddMe = addMe;
                allowedPowers.removeIf(a -> a.getName().equals(finalAddMe.getPowerSelection()));
            }
            if (!addMe.getQualitySelection().isEmpty()) {
                Ability finalAddMe1 = addMe;
                allowedQuals.removeIf(a -> a.getName().equals(finalAddMe1.getQualitySelection()));
            }
            yellowAbls.add(addMe);
        }

        return yellowAbls;
    }


    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getSelectionNumber() {
        return selectionNumber;
    }

    public List<String> getRequiredPowers() {
        return Collections.unmodifiableList(requiredPowers);
    }

    public void setRequiredPowers(List<String> requiredPowers) {
        this.requiredPowers = requiredPowers;
    }

    public void setSelectionNumber(int selectionNumber) {
        this.selectionNumber = selectionNumber;
    }

    public String getName() {
        return name;
    }

    public List<Integer> getNextDice() {
        return nextDice;
    }

    public void setNextDice(List<Integer> nextDice) {
        this.nextDice = nextDice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPowers() {
        return powers;
    }

    public void setPowers(List<String> powers) {
        this.powers = powers;
    }

    public List<Ability> getYellowAbilities() {
        return new ArrayList<>(yellowAbilities);
    }

    public void setYellowAbilities(List<Ability> yellowAbilities) {
        this.yellowAbilities = yellowAbilities;
    }

    public List<Ability> getGreenAbilities() {
        return new ArrayList<>(greenAbilities);
    }

    public void setGreenAbilities(List<Ability> greenAbilities) {
        this.greenAbilities = greenAbilities;
    }

    public int getYellowChoices() {
        return yellowChoices;
    }

    public void setYellowChoices(int yellowChoices) {
        this.yellowChoices = yellowChoices;
    }

    public int getGreenChoices() {
        return greenChoices;
    }

    public void setGreenChoices(int greenChoices) {
        this.greenChoices = greenChoices;
    }

}
