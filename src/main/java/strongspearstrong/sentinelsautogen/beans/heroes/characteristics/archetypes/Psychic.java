package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.heroes.Hero;

import java.util.List;

public class Psychic extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {

        hero.addPowers(assignXFromArchToAttributes(getRequiredPowers(), hero.getPowers(), useDice, 2));
        disperseRemainingDiceToAttributes(getRemainingAnyPowers(), getRemainingAnyQualities(), hero, useDice);

        hero.addGreenAbilities(getXAbilitiesAnyAttributesFromHero(getGreenAbilities(), getGreenChoices(), hero.getPowers(), hero.getQualities()));
        hero.addYellowAbilities(getXAbilitiesAnyAttributesFromHero(getYellowAbilities(), getYellowChoices(), hero.getPowers(), hero.getQualities()));

        return hero;
    }
}
