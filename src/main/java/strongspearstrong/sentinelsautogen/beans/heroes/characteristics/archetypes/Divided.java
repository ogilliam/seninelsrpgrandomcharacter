package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.collections.heroes.ArchetypesCollection;
import strongspearstrong.sentinelsautogen.collections.heroes.PowerSourceCollection;
import strongspearstrong.sentinelsautogen.utils.Rolling;

import java.util.ArrayList;
import java.util.List;

public class Divided extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        List<Attribute> oldPowers = new ArrayList<>(hero.getPowers());
        List<Attribute> oldQualities = new ArrayList<>(hero.getQualities());

        List<Integer> archDice = new ArrayList<>(PowerSourceCollection.getPowerSourceByName(hero.getPowerSource().getName()).getNextDice());
        int res;
        List<Integer> roll = Rolling.rollListIntoList(archDice);
        do {
            res = Rolling.lazyCombinations(roll);
        } while (res == 19 || res == 20);
        AbstractArchetype newArch = ArchetypesCollection.getArchetypeByRoll(res);
        hero.getArchetype().setName("divided - " + newArch.getName());
        hero = newArch.archetypeBuilder(hero, useDice);

        //gets the transformation abilities
        List<Attribute> transformPows = new ArrayList<>(hero.getPowers());
        List<Attribute> transformQuals = new ArrayList<>(hero.getQualities());
        transformPows.removeAll(oldPowers);
        transformQuals.removeAll(oldQualities);

        hero.addGreenAbilities(getXAbilitiesDifferentAttributesFromHero(getGreenAbilities(), getGreenChoices(), transformPows, transformQuals));

        hero.addGreenAbilities(getXAbilitiesDifferentAttributesFromHero(getSecondaryGreenAbilities(), getSecondaryGreenChoices(), hero.getPowers(), hero.getQualities()));
        hero.getPrinciples().remove(1);

        return hero;
    }
}
