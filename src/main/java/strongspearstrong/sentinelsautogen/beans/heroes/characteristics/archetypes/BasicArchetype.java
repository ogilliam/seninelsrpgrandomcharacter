package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.heroes.Hero;

import java.util.List;

public class BasicArchetype extends AbstractArchetype {

    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        return hero;
    }
}
