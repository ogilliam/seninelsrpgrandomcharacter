package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Marksman extends AbstractArchetype {
    @Override
    protected Hero customStepList(Hero hero, List<Integer> useDice) {
        //assigns one die to one of the mandatory powers
        hero.addPowers(assignXFromArchToAttributes(getRequiredPowers(), hero.getPowers(), useDice, 1));

        //assigns one to a remaining any power
        hero.addPowers(assignOneToAttributeList(getRemainingAnyPowers(), hero.getPowers(), useDice));

        disperseRemainingDiceToAttributes(getRemainingAnyPowers(), getRemainingAnyQualities(), hero, useDice);

        //get one explicitly from Signature Weaponry
        List<Ability> gainedGreenAbilities = getXAbilitiesDifferentAttributesFromArchetypeList(getGreenAbilities(), 1,
                new ArrayList<>(Collections.singletonList(new Attribute("signature weaponry", "hallmark"))), new ArrayList<>());

        List<Ability> availGreenAbl = new ArrayList<>(getGreenAbilities());
        availGreenAbl.removeAll(gainedGreenAbilities);
        //get one using a quality
        gainedGreenAbilities.addAll(getXAbilitiesDifferentAttributesFromHero(availGreenAbl, getGreenChoices() - 1,
                new ArrayList<>(), hero.getQualities()));

        hero.addGreenAbilities(gainedGreenAbilities);

        //gain yellow using different qualities
        hero.addYellowAbilities(getXAbilitiesDifferentAttributesFromHero(getYellowAbilities(), getYellowChoices(), new ArrayList<>(), hero.getQualities()));

        return hero;
    }
}
