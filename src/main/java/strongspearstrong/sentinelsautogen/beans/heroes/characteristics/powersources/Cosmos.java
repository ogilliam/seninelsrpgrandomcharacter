package strongspearstrong.sentinelsautogen.beans.heroes.characteristics.powersources;

import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.utils.Rolling;

import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Cosmos extends AbstractPowerSource {

    @Override
    public void extraSteps(Hero hero) {
        List<Attribute> downGrade = hero.getPowers().stream().filter(p -> p.getDieRating() > 6).collect(toList());
        Collections.shuffle(downGrade, Rolling.getRand());
        Attribute downgrader = downGrade.get(0);
        List<Attribute> upgrade = hero.getPowers().stream().filter(p -> p.getDieRating() < 12).collect(toList());
        upgrade.remove(downgrader);
        Collections.shuffle(upgrade, Rolling.getRand());
        Attribute upgrader = upgrade.get(0);

        hero.getPowers().set(hero.getPowers().indexOf(downgrader), new Attribute(downgrader.getName(), downgrader.getCategory(), downgrader.getDieRating() - 2));
        hero.getPowers().set(hero.getPowers().indexOf(upgrader), new Attribute(upgrader.getName(), upgrader.getCategory(), upgrader.getDieRating() - 2));
    }
}
