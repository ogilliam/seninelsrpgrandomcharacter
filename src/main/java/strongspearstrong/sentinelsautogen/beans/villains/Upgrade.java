package strongspearstrong.sentinelsautogen.beans.villains;

public class Upgrade {
    private String name;
    private int healthMod;
    private int pgNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealthMod() {
        return healthMod;
    }

    public void setHealthMod(int healthMod) {
        this.healthMod = healthMod;
    }

    public int getPgNumber() {
        return pgNumber;
    }

    public void setPgNumber(int pgNumber) {
        this.pgNumber = pgNumber;
    }

    @Override
    public String toString() {
        return name + " pg" + pgNumber;
    }
}
