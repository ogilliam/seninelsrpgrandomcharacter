package strongspearstrong.sentinelsautogen.beans.villains.characteristics.approaches;

import strongspearstrong.sentinelsautogen.beans.villains.Villain;

public class BasicApproach extends AbstractApproach{

    @Override
    protected Villain customStepList(Villain villain) {
        villain.addAbilities(getXAbilitiesAnyAttributesFromVillain(villain.getPowers(), villain.getQualities()));
        return villain;
    }
}
