package strongspearstrong.sentinelsautogen.beans.villains.characteristics.approaches;

import strongspearstrong.sentinelsautogen.beans.villains.Villain;

public class Specialized extends AbstractApproach {
    @Override
    protected Villain customStepList(Villain villain) {
        villain.addAbilities(get2AbilitiesSame1DifferentSingleAttributeList(villain.getQualities()));
        return villain;
    }
}
