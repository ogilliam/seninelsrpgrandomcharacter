package strongspearstrong.sentinelsautogen.beans.villains.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.villains.Villain;

public class Legion extends AbstractVArchetype {
    @Override
    protected Villain customStepList(Villain villain) {
        villain.addAbilities(getXAbilitiesAnyAttributesFromVillain(villain.getPowers(), villain.getQualities()));
        villain.addAbility(new Ability("Uncoordinated Actions"));
        return villain;
    }
}
