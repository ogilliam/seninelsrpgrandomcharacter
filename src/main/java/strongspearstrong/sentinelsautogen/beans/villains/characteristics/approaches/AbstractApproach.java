package strongspearstrong.sentinelsautogen.beans.villains.characteristics.approaches;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.villains.Villain;
import strongspearstrong.sentinelsautogen.collections.PowersCollection;
import strongspearstrong.sentinelsautogen.collections.QualitiesCollection;
import strongspearstrong.sentinelsautogen.utils.Rolling;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        defaultImpl = BasicApproach.class,
        property = "name",
        visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Focused.class, name = "focused"),
        @JsonSubTypes.Type(value = Prideful.class, name = "prideful"),
        @JsonSubTypes.Type(value = Specialized.class, name = "specialized"),
        @JsonSubTypes.Type(value = Underpowered.class, name = "underpowered"),
})
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractApproach {
    private int selectionNumber;
    private String name;
    private int pageNumber;
    private int[] powers;
    private int[] qualities;
    private int baseHealth;
    private int abilityChoice;
    private List<Ability> abilityOptions = new ArrayList<>();

    public Villain approachBuilder(Villain myVill) {
        myVill.setPowers(assignPowers());
        myVill.addQuality(new Attribute("Roleplay", "Roleplay", 8));
        myVill.setQualities(assignQualities());
        myVill.addHealth(baseHealth);
        myVill = customStepList(myVill);

        return myVill;
    }

    //each archetype has its own set of steps to use, which much be defined on a per archetype basis
    protected abstract Villain customStepList(Villain villain);

    protected List<Attribute> assignPowers() {
        return extractAttribute(new ArrayList<>(PowersCollection.getPowers()), powers);
    }

    protected List<Attribute> assignQualities() {
        return extractAttribute(new ArrayList<>(QualitiesCollection.getQualities()), qualities);
    }

    private List<Attribute> extractAttribute(ArrayList<Attribute> options, int[] attributeArray) {
        Collections.shuffle(options, Rolling.getRand());
        List<Attribute> villainAttributes = new ArrayList<>();

        for (int i = 0; i < attributeArray.length; i++) {
            villainAttributes.add(new Attribute(options.get(i).getName(), options.get(i).getCategory(), attributeArray[i]));
        }
        return villainAttributes;
    }

    protected List<Ability> getXAbilitiesAnyAttributesFromVillain(List<Attribute> villainPowers, List<Attribute> villainQualities) {
        return new Utils().loopForGoodAbilities(abilityChoice, new ArrayList<>(villainPowers), new ArrayList<>(villainQualities), abilityOptions, true);
    }

    protected List<Ability> getXAbilitiesDifferentAttributesFromApproachList(List<Attribute> villPowers, List<Attribute> villQualities) {
        return new Utils().loopForGoodAbilities(abilityChoice, villPowers, villQualities, abilityOptions, false);
    }

    protected List<Ability> get2AbilitiesSame1DifferentSingleAttributeList(List<Attribute> attribList) {
        Utils utils = new Utils();

        List<Attribute> shuffledList = new ArrayList<>(attribList);
        Collections.shuffle(shuffledList, Rolling.getRand());

        List<Ability> availableAbilities = new ArrayList<>(abilityOptions);
        Collections.shuffle(availableAbilities, Rolling.getRand());
        List<Ability> newAbls = new ArrayList<>(utils.loopForGoodAbilities(2, Collections.singletonList(shuffledList.get(0)),
                Collections.singletonList(shuffledList.get(0)), availableAbilities, true));

        for (Ability a : newAbls) {
            availableAbilities.remove(a);
        }

        newAbls.addAll(utils.loopForGoodAbilities(1, Collections.singletonList(shuffledList.get(1)), Collections.singletonList(shuffledList.get(1)), availableAbilities, true));

        return newAbls;
    }

    public int getSelectionNumber() {
        return selectionNumber;
    }

    public void setSelectionNumber(int selectionNumber) {
        this.selectionNumber = selectionNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int[] getPowers() {
        return powers;
    }

    public void setPowers(int[] powers) {
        this.powers = powers;
    }

    public int[] getQualities() {
        return qualities;
    }

    public void setQualities(int[] qualities) {
        this.qualities = qualities;
    }

    public int getBaseHealth() {
        return baseHealth;
    }

    public void setBaseHealth(int baseHealth) {
        this.baseHealth = baseHealth;
    }

    public int getAbilityChoice() {
        return abilityChoice;
    }

    public void setAbilityChoice(int abilityChoice) {
        this.abilityChoice = abilityChoice;
    }

    public List<Ability> getAbilityOptions() {
        return abilityOptions;
    }

    public void setAbilityOptions(List<Ability> abilityOptions) {
        this.abilityOptions = abilityOptions;
    }
}
