package strongspearstrong.sentinelsautogen.beans.villains.characteristics.archetypes;

import strongspearstrong.sentinelsautogen.beans.villains.Villain;

public class BasicVArchetype extends AbstractVArchetype {

    @Override
    protected Villain customStepList(Villain villain) {
        villain.addAbilities(getXAbilitiesAnyAttributesFromVillain(villain.getPowers(), villain.getQualities()));
        return villain;
    }
}
