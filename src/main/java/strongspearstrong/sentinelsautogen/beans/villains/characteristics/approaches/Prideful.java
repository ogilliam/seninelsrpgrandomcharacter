package strongspearstrong.sentinelsautogen.beans.villains.characteristics.approaches;

import strongspearstrong.sentinelsautogen.beans.villains.Villain;

public class Prideful extends AbstractApproach {
    @Override
    protected Villain customStepList(Villain villain) {
        villain.addAbilities(getXAbilitiesDifferentAttributesFromApproachList(villain.getPowers(), villain.getQualities()));
        return villain;
    }
}
