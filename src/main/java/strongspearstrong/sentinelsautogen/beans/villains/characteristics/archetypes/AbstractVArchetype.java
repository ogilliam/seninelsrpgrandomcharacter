package strongspearstrong.sentinelsautogen.beans.villains.characteristics.archetypes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.villains.Villain;
import strongspearstrong.sentinelsautogen.utils.Utils;

import java.util.ArrayList;
import java.util.List;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        defaultImpl = BasicVArchetype.class,
        property = "name",
        visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Legion.class, name = "legion"),
})
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractVArchetype {
    private int selectionNumber;
    private String name;
    private int pageNumber;
    private int healthBonus;
    private int abilityChoice;
    private List<Ability> abilityOptions = new ArrayList<>();


    public Villain vArchetypeBuilder(Villain myVill) {
        myVill.addHealth(healthBonus);
        myVill = customStepList(myVill);

        return myVill;
    }

    protected abstract Villain customStepList(Villain villain);

    protected List<Ability> getXAbilitiesAnyAttributesFromVillain(List<Attribute> villainPowers, List<Attribute> villainQualities) {
        return new Utils().loopForGoodAbilities(abilityChoice, new ArrayList<>(villainPowers), new ArrayList<>(villainQualities), abilityOptions, true);
    }

    public int getSelectionNumber() {
        return selectionNumber;
    }

    public void setSelectionNumber(int selectionNumber) {
        this.selectionNumber = selectionNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getHealthBonus() {
        return healthBonus;
    }

    public void setHealthBonus(int healthBonus) {
        this.healthBonus = healthBonus;
    }

    public int getAbilityChoice() {
        return abilityChoice;
    }

    public void setAbilityChoice(int abilityChoice) {
        this.abilityChoice = abilityChoice;
    }

    public List<Ability> getAbilityOptions() {
        return abilityOptions;
    }

    public void setAbilityOptions(List<Ability> abilityOptions) {
        this.abilityOptions = abilityOptions;
    }
}
