package strongspearstrong.sentinelsautogen.beans.villains;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.Characteristic;

import java.util.ArrayList;
import java.util.List;

public class Villain {
    private Characteristic approach;
    private Characteristic archetype;
    private List<Attribute> powers = new ArrayList<>();
    private List<Attribute> qualities = new ArrayList<>();
    private List<Ability> abilities = new ArrayList<>();
    private List<Upgrade> upgrades = new ArrayList<>();
    private String mastery;
    private int health = 0;

    private long seed;

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public Characteristic getApproach() {
        return approach;
    }

    public void setApproach(Characteristic approach) {
        this.approach = approach;
    }

    public Characteristic getArchetype() {
        return archetype;
    }

    public void setArchetype(Characteristic archetype) {
        this.archetype = archetype;
    }

    public List<Attribute> getPowers() {
        return powers;
    }

    public void setPowers(List<Attribute> powers) {
        this.powers = powers;
    }

    public void addPower(Attribute pow) {
        this.powers.add(pow);
    }

    public void addPowers(List<Attribute> powers) {
        this.powers.addAll(powers);
    }

    public List<Attribute> getQualities() {
        return qualities;
    }

    public void setQualities(List<Attribute> qualities) {
        this.qualities = qualities;
    }

    public void addQuality(Attribute qual) {
        this.qualities.add(qual);
    }

    public void addQualities(List<Attribute> qual) {
        this.qualities.addAll(qual);
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<Ability> abilities) {
        this.abilities = abilities;
    }

    public void addAbilities(List<Ability> abl) {
        this.abilities.addAll(abl);
    }

    public void addAbility(Ability abl) {
        this.abilities.add(abl);
    }

    public List<Upgrade> getUpgrades() {
        return upgrades;
    }

    public void setUpgrades(List<Upgrade> upgrades) {
        this.upgrades = upgrades;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void addHealth(int healthMod){
        this.health += healthMod;
    }

    public String getMastery() {
        return mastery;
    }

    public void setMastery(String mastery) {
        this.mastery = mastery;
    }
}
