package strongspearstrong.sentinelsautogen.beans;

public class Characteristic {
    String name;
    int pgNumber;

    public Characteristic(String name, int pgNumber) {
        this.name = name;
        this.pgNumber = pgNumber;
    }

    public String getName() {
        return name;
    }

    public int getPgNumber() {
        return pgNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPgNumber(int pgNumber) {
        this.pgNumber = pgNumber;
    }

    @Override
    public String toString() {
        return name + " pg" + pgNumber;
    }

}
