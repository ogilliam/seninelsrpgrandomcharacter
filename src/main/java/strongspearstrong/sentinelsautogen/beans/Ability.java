package strongspearstrong.sentinelsautogen.beans;

public class Ability {
    private String name;
    private String powerSelection;
    private String qualitySelection;
    private String boostSelection;

    public Ability(String name, String powerSelection, String qualitySelection, String boostSelection) {
        this.name = name;
        this.powerSelection = powerSelection;
        this.qualitySelection = qualitySelection;
        this.boostSelection = boostSelection;
    }

    public Ability(String name) {
        this.name = name;
        this.powerSelection = "";
        this.qualitySelection = "";
        this.boostSelection = "";
    }

    public Ability(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPowerSelection() {
        return powerSelection;
    }

    public void setPowerSelection(String powerSelection) {
        this.powerSelection = powerSelection;
    }

    public String getQualitySelection() {
        return qualitySelection;
    }

    public void setQualitySelection(String qualitySelection) {
        this.qualitySelection = qualitySelection;
    }

    public String getBoostSelection() {
        return boostSelection;
    }

    public void setBoostSelection(String boostSelection) {
        this.boostSelection = boostSelection;
    }

    @Override
    public String toString() {
        return name + " " + getTraitSelection();
    }

    public String forHtml(){
        String retString = name;
        if(!getTraitSelection().isEmpty()){
            retString += "    -    " ;
            retString += getTraitSelection();
        }
        return retString;
    }

    public String getTraitSelection(){
        String retString = "";
        if(!powerSelection.isEmpty()) retString += getPowerSelection() + " ";
        if(!qualitySelection.isEmpty()) retString += getQualitySelection() + " ";
        if(!boostSelection.isEmpty()) retString += getBoostSelection();

        return retString;
    }

    @Override
    public boolean equals(Object o){
        if(!(o instanceof Ability)){
            return false;
        }

        if(o == this){
            return true;
        }
        return this.name.equals(((Ability) o).getName());
    }
}
