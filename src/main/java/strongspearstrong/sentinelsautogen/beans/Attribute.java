package strongspearstrong.sentinelsautogen.beans;

public class Attribute {
    private String name;
    private String category;
    private int dieRating;

    public Attribute(String name, String category, int dieRating) {
        this.name = name;
        this.category = category;
        this.dieRating = dieRating;
    }


    public Attribute(String name, String category) {
        this.name = name;
        this.category = "";
        this.dieRating = 0;
    }

    public Attribute(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDieRating() {
        return dieRating;
    }

    public void setDieRating(int dieRating) {
        this.dieRating = dieRating;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return name + " d" + dieRating;
    }

    @Override
    public boolean equals(Object o){
        if(!(o instanceof Attribute)){
            return false;
        }

        if(o == this){
            return true;
        }
        return this.name.equals(((Attribute) o).getName());
    }
}
