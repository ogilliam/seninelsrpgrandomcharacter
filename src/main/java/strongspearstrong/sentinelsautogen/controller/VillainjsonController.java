package strongspearstrong.sentinelsautogen.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import strongspearstrong.sentinelsautogen.html.GenHTMLVillain;
import strongspearstrong.sentinelsautogen.steps.CreateVillain;

@RestController
public class VillainjsonController {

    private final ObjectMapper om = new ObjectMapper();

    @GetMapping("/newvillainbland")
    public String newvillain() {
        return new GenHTMLVillain().getMyHTML(new CreateVillain().createNewVillainUnseeded());
    }

    @GetMapping("/newvillainbland/{seed}")
    public String newvillainSeeded(@PathVariable(value = "seed") long seed) {
        return new GenHTMLVillain().getMyHTML(new CreateVillain().createNewVillainSeeded(seed));
    }

    @GetMapping("/newvillainjson")
    public String newvillainJson() throws JsonProcessingException {
        return om.writerWithDefaultPrettyPrinter().writeValueAsString(new CreateVillain().createNewVillainUnseeded());
    }

    @GetMapping("/newvillainjson/{seed}")
    public String newvillainJsonSeeded(@PathVariable(value = "seed") long seed) throws JsonProcessingException {
        return om.writerWithDefaultPrettyPrinter().writeValueAsString(new CreateVillain().createNewVillainSeeded(seed));
    }

}

