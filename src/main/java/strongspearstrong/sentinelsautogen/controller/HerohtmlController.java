package strongspearstrong.sentinelsautogen.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.steps.CreateHero;

@Controller
public class HerohtmlController {

    private final ObjectMapper om = new ObjectMapper();

    @GetMapping("/newhero/{seed}")
    public String newHeroSeeded(@PathVariable(value = "seed") long seed, Model model) {
        Hero hero = new CreateHero().createNewHeroSeeded(seed);
        model.addAttribute("hero", hero);
        return "herohtml";
    }

    @GetMapping("/newhero")
    public String herohtml(Model model) {
        Hero hero = new CreateHero().createNewHeroUnseeded();
        model.addAttribute("hero", hero);
        return "herohtml";
    }

}
