package strongspearstrong.sentinelsautogen.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import strongspearstrong.sentinelsautogen.html.GenHTMLHero;
import strongspearstrong.sentinelsautogen.steps.CreateHero;

@RestController
public class HerojsonController {

    private final ObjectMapper om = new ObjectMapper();

    @GetMapping("/newherobland")
    public String newHero() {
        return new GenHTMLHero().getMyHTML(new CreateHero().createNewHeroUnseeded());
    }

    @GetMapping("/newherobland/{seed}")
    public String newHeroSeeded(@PathVariable(value = "seed") long seed) {
        return new GenHTMLHero().getMyHTML(new CreateHero().createNewHeroSeeded(seed));
    }

    @GetMapping("/newherojson")
    public String newHeroJson() throws JsonProcessingException {
        return om.writerWithDefaultPrettyPrinter().writeValueAsString(new CreateHero().createNewHeroUnseeded());
    }

    @GetMapping("/newherojson/{seed}")
    public String newHeroJsonSeeded(@PathVariable(value = "seed") long seed) throws JsonProcessingException {
        return om.writerWithDefaultPrettyPrinter().writeValueAsString(new CreateHero().createNewHeroSeeded(seed));
    }
}


