package strongspearstrong.sentinelsautogen.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import strongspearstrong.sentinelsautogen.beans.villains.Villain;
import strongspearstrong.sentinelsautogen.steps.CreateVillain;

@Controller
public class VillainhtmlController {

    private final ObjectMapper om = new ObjectMapper();

    @GetMapping("/newvillain/{seed}")
    public String newvillainSeeded(@PathVariable(value = "seed") long seed, Model model) {
        Villain villain = new CreateVillain().createNewVillainSeeded(seed);
        model.addAttribute("villain", villain);
        return "villainhtml";
    }

    @GetMapping("/newvillain")
    public String villainhtml(Model model) {
        Villain villain = new CreateVillain().createNewVillainUnseeded();
        model.addAttribute("villain", villain);
        return "villainhtml";
    }

}