package strongspearstrong.sentinelsautogen.steps;

import strongspearstrong.sentinelsautogen.beans.Characteristic;
import strongspearstrong.sentinelsautogen.beans.villains.Upgrade;
import strongspearstrong.sentinelsautogen.beans.villains.Villain;
import strongspearstrong.sentinelsautogen.beans.villains.characteristics.approaches.AbstractApproach;
import strongspearstrong.sentinelsautogen.beans.villains.characteristics.archetypes.AbstractVArchetype;
import strongspearstrong.sentinelsautogen.collections.villains.ApproachesCollection;
import strongspearstrong.sentinelsautogen.collections.villains.MasteriesCollection;
import strongspearstrong.sentinelsautogen.collections.villains.UpgradesCollection;
import strongspearstrong.sentinelsautogen.collections.villains.VArchetypeCollection;
import strongspearstrong.sentinelsautogen.utils.Rolling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CreateVillain {

    public Villain createNewVillainSeeded(long seed) {
        Rolling.setSeed(seed);
        return createNewVillain(seed);
    }

    public Villain createNewVillainUnseeded() {
        long seed = new Random().nextLong();
        Rolling.setSeed(seed);
        return createNewVillain(seed);
    }

    private Villain createNewVillain(long seed) {
        Villain myVill = new Villain();
        myVill.setSeed(seed);

        AbstractApproach myApp = ApproachesCollection.getApproachByRoll(Rolling.rollMe(ApproachesCollection.getApproaches().size()));
        myVill.setApproach(new Characteristic(myApp.getName(), myApp.getPageNumber()));
        myVill = myApp.approachBuilder(myVill);

        AbstractVArchetype myArch = VArchetypeCollection.getVArchetypeByRoll(Rolling.rollMe(VArchetypeCollection.getvArchetypees().size()));
        myVill.setArchetype(new Characteristic(myArch.getName(), myArch.getPageNumber()));
        myVill = myArch.vArchetypeBuilder(myVill);

        int upgradeNumber = Rolling.rollMe(UpgradesCollection.getUpgrades().size() + 1) - 1;

        if (upgradeNumber > 0) {
            List<Upgrade> upgrades = new ArrayList<>(UpgradesCollection.getUpgrades());
            List<Upgrade> newUpgrades = new ArrayList<>();
            Collections.shuffle(upgrades, Rolling.getRand());

            for (int i = 0; i < upgradeNumber; i++) {
                newUpgrades.add(upgrades.get(i));
                myVill.addHealth(upgrades.get(i).getHealthMod());
            }
            myVill.setUpgrades(newUpgrades);
        }

        if (myVill.getUpgrades().size() > 0) {
            myVill.setMastery(MasteriesCollection.getMasteryByRoll(Rolling.rollMe(MasteriesCollection.getMasteries().size() - 1)).getName());
        }

        return myVill;
    }
}
