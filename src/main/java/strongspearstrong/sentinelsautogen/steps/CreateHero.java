package strongspearstrong.sentinelsautogen.steps;

import strongspearstrong.sentinelsautogen.beans.Characteristic;
import strongspearstrong.sentinelsautogen.beans.heroes.Hero;
import strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes.AbstractArchetype;
import strongspearstrong.sentinelsautogen.beans.heroes.characteristics.backgrounds.AbstractBackground;
import strongspearstrong.sentinelsautogen.beans.heroes.characteristics.personalities.AbstractPersonality;
import strongspearstrong.sentinelsautogen.beans.heroes.characteristics.powersources.AbstractPowerSource;
import strongspearstrong.sentinelsautogen.collections.heroes.ArchetypesCollection;
import strongspearstrong.sentinelsautogen.collections.heroes.BackgroundsCollection;
import strongspearstrong.sentinelsautogen.collections.heroes.PersonalitiesCollection;
import strongspearstrong.sentinelsautogen.collections.heroes.PowerSourceCollection;
import strongspearstrong.sentinelsautogen.utils.Rolling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class CreateHero {

    public Hero createNewHeroSeeded(long seed){
        Rolling.setSeed(seed);
        return createNewHero(seed);
    }

    public Hero createNewHeroUnseeded(){
        long seed = new Random().nextLong();
        Rolling.setSeed(seed);
        return createNewHero(seed);
    }

    private Hero createNewHero(long seed) {
        Hero myHero = new Hero();
        myHero.setSeed(seed);

        System.out.println(Rolling.getSeed());
        //Rolls initial 2d10 for selecting a background and initiates the background phase
        List<Integer> rolledDice = new ArrayList<>(Arrays.asList(Rolling.rollMe(10), Rolling.rollMe(10)));
        AbstractBackground myBack = BackgroundsCollection.getBackgroundByRoll(Rolling.lazyCombinations(rolledDice));
        myHero.setBackground(new Characteristic(myBack.getName(), myBack.getPageNumber()));
        myHero = myBack.backgroundBuilder(myHero);


        //Gets the sizes and numbers of dice from the background phase, rolls them for powersource selection
        rolledDice = Rolling.rollListIntoList(new ArrayList<>(myBack.getNextDice()));
        AbstractPowerSource myPS = PowerSourceCollection.getPowerSourceByRoll(Rolling.lazyCombinations(rolledDice));
        myHero.setPowerSource(new Characteristic(myPS.getName(), myPS.getPageNumber()));
        myHero = myPS.powerSourceBuilder(myHero, new ArrayList<>(myBack.getNextDice()));

        //gets for archetype phase
        rolledDice = Rolling.rollListIntoList(new ArrayList<>(myPS.getNextDice()));
        AbstractArchetype myArch = ArchetypesCollection.getArchetypeByRoll(Rolling.lazyCombinations(rolledDice));
        myHero.setArchetype(new Characteristic(myArch.getName(), myArch.getPageNumber()));
        myHero = myArch.archetypeBuilder(myHero, new ArrayList<>(myPS.getNextDice()));


        rolledDice = Rolling.rollListIntoList(Arrays.asList(Rolling.rollMe(10), Rolling.rollMe(10)));
        //gets for personality phase
        AbstractPersonality myPers = PersonalitiesCollection.getPersonalityByRoll(Rolling.lazyCombinations(rolledDice));
        myHero.setPersonality(new Characteristic(myPers.getName(), myPers.getPageNumber()));
        myHero = myPers.personalityBuilder(myHero);


        return myHero;
    }
}
