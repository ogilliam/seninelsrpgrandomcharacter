package strongspearstrong.sentinelsautogen.html;

import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.beans.heroes.Principle;
import strongspearstrong.sentinelsautogen.beans.villains.Upgrade;

import java.util.List;

public class HtmlUtils {
    public String tableD(String s, String t) {
        return "<tr><td>" + s + "</td> \n<td>" + t + "</td></tr>\n";
    }

    public String abils(List<Ability> myAbils, String color) {
        StringBuilder retSt = new StringBuilder();
        for (Ability a : myAbils) {
            retSt.append(tableD(color, a.toString()));
        }

        return retSt.toString();
    }

    public String stats(List<Attribute> atrib, String type) {
        StringBuilder retSt = new StringBuilder();
        for (Attribute a : atrib) {
            retSt.append(tableD(type, a.toString()));
        }
        return retSt.toString();
    }

    public String upgrade(List<Upgrade> atrib, String type) {
        StringBuilder retSt = new StringBuilder();
        for (Upgrade a : atrib) {
            retSt.append(tableD(type, a.toString()));
        }
        return retSt.toString();
    }

    public String princ(List<Principle> princ) {
        StringBuilder retSt = new StringBuilder();
        for (Principle s : princ) {
            retSt.append(tableD("principle", s.toString()));
        }
        return retSt.toString();
    }
}
