package strongspearstrong.sentinelsautogen.html;

import strongspearstrong.sentinelsautogen.beans.heroes.Hero;

public class GenHTMLHero {

    private final HtmlUtils utils = new HtmlUtils();
    
    public String getMyHTML(Hero hero) {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>New Hero</title>\n" +
                "</head>\n" +
                "<body>\n" +
                heroGen(hero) +
                "</body>\n" +
                "</html>";
    }

    private String heroGen(Hero hero) {
        String startTable = "<table>\n";
        String seed = utils.tableD("SEED USED: ", Long.toString(hero.getSeed()));
        String background = utils.tableD("background", hero.getBackground().toString());
        String source = utils.tableD("power source", hero.getPowerSource().toString());
        String archetype = utils.tableD("archetype", hero.getArchetype().toString());
        String personality = utils.tableD("personality", hero.getPersonality().toString());
        String princ = utils.princ(hero.getPrinciples());
        String powers = utils.stats(hero.getPowers(), "power");
        String qualities = utils.stats(hero.getQualities(), "quality");
        String greenAbil = utils.abils(hero.getGreenAbilities(), "green");
        String yellowAbil = utils.abils(hero.getYellowAbilities(), "yellow");
        String redAbil = utils.abils(hero.getRedAbilities(), "red");
        String endTable = "</table>";

        return startTable + seed + background + source + archetype + personality + princ + powers + qualities + greenAbil + yellowAbil + redAbil + endTable;
    }

}
