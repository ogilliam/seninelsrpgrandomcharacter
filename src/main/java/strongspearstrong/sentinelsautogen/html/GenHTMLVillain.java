package strongspearstrong.sentinelsautogen.html;

import strongspearstrong.sentinelsautogen.beans.villains.Villain;

public class GenHTMLVillain {
    
    private final HtmlUtils utils = new HtmlUtils();

    public String getMyHTML(Villain villain) {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>New Villain</title>\n" +
                "</head>\n" +
                "<body>\n" +
                villainGen(villain) +
                "</body>\n" +
                "</html>";
    }

    private String villainGen(Villain villain) {
        String startTable = "<table>\n";
        String seed = utils.tableD("SEED USED: ", Long.toString(villain.getSeed()));
        String baseHealth = utils.tableD("base health", String.valueOf(villain.getHealth()));
        String approach = utils.tableD("approach", villain.getApproach().toString());
        String archetype = utils.tableD("archetype", villain.getArchetype().toString());
        String powers = utils.stats(villain.getPowers(), "power");
        String qualities = utils.stats(villain.getQualities(), "quality");
        String abilities = utils.abils(villain.getAbilities(), "abilities");
        String mastery = utils.tableD("mastery", villain.getMastery());
        String upgrades = utils.upgrade(villain.getUpgrades(), "upgrades");
        String endTable = "</table>";

        return startTable + seed + baseHealth + approach + archetype + powers + qualities + abilities + mastery + upgrades + endTable;
    }
    
}
