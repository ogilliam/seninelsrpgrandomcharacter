package strongspearstrong.sentinelsautogen.collections;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Component
public class PowersCollection {
    private static List<Attribute> powers;

    @Bean
    private void collectPowers() throws IOException {
        powers = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/hero/powers.json"), new TypeReference<>() {
        });
    }

    public static List<Attribute> getPowers() {
        return Collections.unmodifiableList(powers);
    }

    public static List<Attribute> getPowersByCat(String cat) {
        Object o = powers.stream().filter(p -> p.getCategory().equals(cat)).collect(toList());
        return powers.stream().filter(p -> p.getCategory().equals(cat)).collect(toList());
    }

    public static Attribute getPowerByName(String name) {
        Optional<Attribute> retPow = powers.stream().filter(p -> p.getName().equals(name)).findFirst();
        if (retPow.isEmpty()) {
            System.out.println("requested: " + name + " from powers and could not find");
        }
        return retPow.orElseGet(Attribute::new);
    }
}
