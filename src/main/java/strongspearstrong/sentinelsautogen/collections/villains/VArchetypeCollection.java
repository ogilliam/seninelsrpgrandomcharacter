package strongspearstrong.sentinelsautogen.collections.villains;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.villains.characteristics.archetypes.AbstractVArchetype;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class VArchetypeCollection {

    private static List<AbstractVArchetype> vArchetypes;

    @Bean
    private void collectvArchetypees() throws IOException {
        vArchetypes = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/villain/vArchetypes.json")
                , new TypeReference<>() {
                });
        vArchetypes.sort(Comparator.comparing(AbstractVArchetype::getSelectionNumber));
    }

    public static List<AbstractVArchetype> getvArchetypees() {
        return new ArrayList<>(vArchetypes);
    }

    public static AbstractVArchetype getVArchetypeByRoll(int arch) {
        return vArchetypes.get(arch - 1);
    }
    
}
