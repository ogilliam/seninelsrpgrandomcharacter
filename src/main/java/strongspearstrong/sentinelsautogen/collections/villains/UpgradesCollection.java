package strongspearstrong.sentinelsautogen.collections.villains;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.villains.Upgrade;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Component
public class UpgradesCollection {

    private static List<Upgrade> upgrades;

    @Bean
    private void collectUpgrades() throws IOException {
        upgrades = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/villain/upgrades.json")
                , new TypeReference<>() {
                });
    }

    public static List<Upgrade> getUpgrades() {
        return Collections.unmodifiableList(upgrades);
    }
}
