package strongspearstrong.sentinelsautogen.collections.villains;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.villains.Mastery;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Component
public class MasteriesCollection {

    private static List<Mastery> masteries;

    @Bean
    private void collectMasteries() throws IOException {
        masteries = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/villain/masteries.json")
                , new TypeReference<>() {
                });
    }

    public static List<Mastery> getMasteries() {
        return Collections.unmodifiableList(masteries);
    }

    public static Mastery getMasteryByRoll(int arch) {
        return masteries.get(arch - 1);
    }
}
