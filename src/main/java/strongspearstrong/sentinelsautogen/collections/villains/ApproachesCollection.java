package strongspearstrong.sentinelsautogen.collections.villains;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.villains.characteristics.approaches.AbstractApproach;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class ApproachesCollection {

    private static List<AbstractApproach> Approaches;

    @Bean
    private void collectApproaches() throws IOException {
        Approaches = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/villain/approaches.json")
                , new TypeReference<>() {
                });
        Approaches.sort(Comparator.comparing(AbstractApproach::getSelectionNumber));
    }

    public static List<AbstractApproach> getApproaches() {
        return new ArrayList<>(Approaches);
    }

    public static AbstractApproach getApproachByRoll(int arch) {
        return Approaches.get(arch - 1);
    }
}
