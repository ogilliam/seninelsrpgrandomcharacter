package strongspearstrong.sentinelsautogen.collections.heroes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.heroes.Principle;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class PrinciplesCollection {
    private static Map<String, List<Principle>> principles;

    @Bean
    private void collectPrinciples() throws IOException {
        principles = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/hero/principles.json")
                , new TypeReference<>() {
                });
    }

    public static List<Principle> getPrinciplesByCat(String cat) {
        return new ArrayList<>(principles.get(cat));
    }

    public static Map<String, List<Principle>> getPrinciples() {
        return Collections.unmodifiableMap(principles);
    }
}
