package strongspearstrong.sentinelsautogen.collections.heroes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.heroes.characteristics.personalities.AbstractPersonality;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class PersonalitiesCollection {
    private static List<AbstractPersonality> personalities;

    @Bean
    private void collectionPersonalities() throws IOException {
        personalities = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/hero/personalities.json")
                , new TypeReference<>() {
                });
        personalities.sort(Comparator.comparing(AbstractPersonality::getSelectionNumber));
    }

    public static List<AbstractPersonality> getPersonalities() {
        return new ArrayList<>(personalities);
    }

    public static AbstractPersonality getPersonalityByRoll(int per) {
        return personalities.get(per - 1);
    }
}
