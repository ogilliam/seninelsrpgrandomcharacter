package strongspearstrong.sentinelsautogen.collections.heroes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.heroes.characteristics.archetypes.AbstractArchetype;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class ArchetypesCollection {

    private static List<AbstractArchetype> archetypes;


    @Bean
    private void collectArchetypes() throws IOException {
        archetypes = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/hero/archetypes.json")
                , new TypeReference<>() {
                });
        archetypes.sort(Comparator.comparing(AbstractArchetype::getSelectionNumber));
    }

    public static List<AbstractArchetype> getArchetypes() {
        return new ArrayList<>(archetypes);
    }

    public static AbstractArchetype getArchetypeByRoll(int arch) {
        return archetypes.get(arch - 1);
    }
}
