package strongspearstrong.sentinelsautogen.collections.heroes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.heroes.characteristics.backgrounds.AbstractBackground;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class BackgroundsCollection {

    private static List<AbstractBackground> backgrounds;

    @Bean
    private void collectBackground() throws IOException {
        backgrounds = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/hero/backgrounds.json")
                , new TypeReference<>() {
                });
        backgrounds.sort(Comparator.comparing(AbstractBackground::getSelectionNumber));
    }

    public static List<AbstractBackground> getBackgrounds() {
        return new ArrayList<>(backgrounds);
    }

    public static AbstractBackground getBackgroundByRoll(int back) {
        return backgrounds.get(back - 1);
    }

}
