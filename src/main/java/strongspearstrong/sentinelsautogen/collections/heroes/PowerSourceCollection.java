package strongspearstrong.sentinelsautogen.collections.heroes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.heroes.characteristics.powersources.AbstractPowerSource;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Component
public class PowerSourceCollection {

    private static List<AbstractPowerSource> powerSources;

    @Bean
    private void collectPowerSources() throws IOException {
        powerSources = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/hero/powersources.json")
                , new TypeReference<>() {
                });
        powerSources.sort(Comparator.comparing(AbstractPowerSource::getSelectionNumber));
    }

    public static List<AbstractPowerSource> getPowerSources() {
        return new ArrayList<>(powerSources);
    }

    public static AbstractPowerSource getPowerSourceByRoll(int ps) {
        return powerSources.get(ps - 1);
    }

    public static AbstractPowerSource getPowerSourceByName(String name){
        Optional<AbstractPowerSource> retVal = powerSources.stream().filter(p -> p.getName().equals(name)).findFirst();
        return retVal.orElseThrow();
    }
}
