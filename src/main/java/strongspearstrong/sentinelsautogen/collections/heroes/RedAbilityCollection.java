package strongspearstrong.sentinelsautogen.collections.heroes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.Ability;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class RedAbilityCollection {
    private static Map<String, List<Ability>> redAbilities;

    @Bean
    private void collectRedAbilities() throws IOException {
        redAbilities = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/hero/redabilities.json"), new TypeReference<>() {
        });
    }

    public static Map<String, List<Ability>> getRedAbilities() {
        return Collections.unmodifiableMap(redAbilities);
    }

    public static List<Ability> getRedAbilitiesByCategory(String cat) {
        List<Ability> retAbl = redAbilities.get(cat);
        return retAbl != null ? new ArrayList<>(retAbl) : new ArrayList<>();
    }


}
