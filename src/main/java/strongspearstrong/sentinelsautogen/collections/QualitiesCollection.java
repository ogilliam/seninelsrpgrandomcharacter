package strongspearstrong.sentinelsautogen.collections;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import strongspearstrong.sentinelsautogen.beans.Attribute;
import strongspearstrong.sentinelsautogen.utils.JsonLoader;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class QualitiesCollection {
    private static List<Attribute> qualities;

    @Bean
    private void collectQualities() throws IOException {
        qualities = new ObjectMapper().readValue(new JsonLoader().getJsonFromFile("datasets/hero/qualities.json"), new TypeReference<>() {
        });
    }

    public static List<Attribute> getQualities() {
        return Collections.unmodifiableList(qualities);
    }

    public static List<Attribute> getQualitiesByCat(String cat) {
        return qualities.stream().filter(p -> p.getCategory().equals(cat)).collect(java.util.stream.Collectors.toList());
    }

    public static Attribute getQualityByName(String name) {
        Optional<Attribute> retQual = qualities.stream().filter(p -> p.getName().equals(name)).findFirst();
        if (retQual.isEmpty()) {
            System.out.println("requested: " + name + " from qualities and could not find");
        }
        return retQual.orElseGet(Attribute::new);
    }
}
